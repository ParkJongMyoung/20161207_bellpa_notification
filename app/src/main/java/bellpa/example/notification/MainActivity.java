package bellpa.example.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {

    private Button button_noti;
    private Context mContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        button_noti = (Button) findViewById(R.id.button_noti);
        button_noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RemoteViews contentiew = new RemoteViews(mContext.getPackageName(), R.layout.view_small_noti_nokey);
                RemoteViews bigView = new RemoteViews(mContext.getPackageName(), R.layout.view_big_noti_nokey);

                Notification foregroundNote;
                Notification.Builder mNotifyBuilder = new Notification.Builder(mContext);

                Intent intentMain = new Intent(mContext, FakeActivity.class);
                PendingIntent pintent = PendingIntent.getActivity(mContext, 0, intentMain, 0);

                bigView.setOnClickPendingIntent(R.id.button_1, pintent);
                bigView.setOnClickPendingIntent(R.id.button_2, pintent);
                bigView.setOnClickPendingIntent(R.id.button_3, pintent);

                foregroundNote = mNotifyBuilder
                        .setSmallIcon(R.drawable.icon_todan)
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setAutoCancel(true)
                        .setContentIntent(pintent)
                        .build();

                foregroundNote.contentView = contentiew;
                foregroundNote.contentView.setTextViewText(R.id.textview_small_noti, "노티의 String");
                foregroundNote.bigContentView = bigView;
                foregroundNote.bigContentView.setTextViewText(R.id.textview_big_1, "큰 거 첫번째");
                foregroundNote.bigContentView.setTextViewText(R.id.textview_big_2, "큰 거 두번째");
                foregroundNote.bigContentView.setTextViewText(R.id.textview_big_3, "큰 거 세번째");


                NotificationManager mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotifyManager.notify(1, foregroundNote);
            }
        });
    }
}
