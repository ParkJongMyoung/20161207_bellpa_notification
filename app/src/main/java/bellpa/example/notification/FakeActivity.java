package bellpa.example.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

public class FakeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.finishAffinity(this);

        Intent intent = new Intent(FakeActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }

}
